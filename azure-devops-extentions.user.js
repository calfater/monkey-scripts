// ==UserScript==
// @name    Devops extentions
// @namespace   Violentmonkey Scripts
// @match     https://dev.azure.com/*
// @grant     none
// @version   2.0
// @author    Sébastien Borie
// @description This extention rewords hours to days and generates beautifull links when using Ctrl + C inside a page.
// ==/UserScript==

'use strict';

(function () {

    console.log('Devops extentions : loading...');

    const conf = {
        h2d: [
            {field: "[field='.WORK_ROLLUP']", search: ' h', replace: ' d'},
            {field: ".progress-text", search: ' h', replace: ' d'},
            {field: "#taskboard-table-header .witRemainingWork", search: ' h', replace: ' d'},
            {field: ".witStateSummary .witRemainingWork", search: ' h', replace: ' d'},
            {field: "[aria-label='Effort (Hours) section.']", search: 'Hours', replace: 'Days'}
        ],
        workItemUrl: window.location.origin + window.location.pathname.split('/').slice(0, 3).join('/') + '/_workitems/edit/${workitemId}',
        messageDisplayTime: 2500,
        includesItemId: true,
        showMore: {
            label : 'Show more'
        },
        debug: false
    }

    /*** hoursToDays ***/

    function loadHoursToDays() {
        setInterval(hoursToDays, 1000);
        console.log('Devops extentions : [hoursToDays  ] : activated');
    }

    function hoursToDays() {
        for (let c of conf.h2d) {
            document.querySelectorAll(c.field).forEach((elt) => {
                if (elt.innerHTML.includes(c.search)) {
                    conf.debug && console.log(`Devops extentions : [hoursToDays  ] : Match ${c.field} ('${c.search}' -> '${c.replace}')`, elt);
                    elt.innerHTML = elt.innerHTML.replace(c.search, c.replace);
                }
            });
        }
    }

    /*** end hoursToDays ***/

    /*** bootstrapCopy ***/

    function getBreadcrumb(depth = 3, selector = null) {
        return Array.from(document.querySelectorAll(null !== selector ? selector : '.bolt-breadcrumb-list-item .text-ellipsis')).slice(depth).map(elem => { return elem.innerText.replace('', ''); }).join(' > ')
    }

    function bootstrapCopy() {

        const copyLinkNode = Object.assign(document.createElement('a'), {id: 'copyLink', href: '#', style: 'position: absolute; z-index: 0;', text: 'Ticket name'});
        document.body.appendChild(copyLinkNode)

        const copyMessageNode = Object.assign(document.createElement('div'), {id: 'copyMessage', style: 'padding-top: 10px; position: absolute; z-index: 11000; width: 100%; text-align: center;', hidden: 'hidden'})
        const copyMessageTextNode = Object.assign(document.createElement('span'), {id: 'copyMessageText', style: 'margin-top: 2px; background-color: rgb(171, 218, 255); padding: 10px; border-radius: 10px', text: 'Ticket name'})
        copyMessageNode.appendChild(copyMessageTextNode);
        document.body.appendChild(copyMessageNode);

        document.addEventListener("copy", (event) => {

            const selection = window.getSelection()
            const activeElement = document.activeElement

            const isSelectionInProgress = !selection.isCollapsed
            const isInputSelected = activeElement.contentEditable === "true" || activeElement.type === "text" || activeElement.type === "textarea"
            const showingPage = {
                editWorkitem : window.location.pathname.includes('/_workitems/'),
                workitem : window.location.search.includes('workitem'),
                workitemFromQuery : window.location.pathname.includes('/_queries/edit/'),
                sprintBoard : window.location.pathname.includes('/_sprints/'),
                queries : window.location.pathname.includes('/_queries/'),
                queriesList : window.location.pathname.endsWith('/_queries'),
                wiki : window.location.pathname.includes('/_wiki'),
                release : window.location.pathname.endsWith('/_release'),
                releaseProgress : window.location.pathname.endsWith('/_releaseProgress')
            }

            conf.debug && console.log('Devops extentions : [bootstrapCopy] : ', showingPage);

            if (!isSelectionInProgress && !isInputSelected) {

                let href = window.location.href
                let text = ""


                if (showingPage.editWorkitem) {
                    const workitemId = window.location.pathname.replace(/.*\//g, '')
                    text = (true === conf.includesItemId ? `#${workitemId} ` : '') + document.querySelector('[aria-label="Title Field"]').value
                } else if (showingPage.workitem) {
                    const workitemId = window.location.search.replace(/.*workitem=/g, '').replace(/[^0-9]/g, '')
                    href = conf.workItemUrl.replace('${workitemId}', workitemId);
                    text = (true === conf.includesItemId ? `#${workitemId} ` : '') + document.querySelector('[aria-label="Title Field"]').value
                } else if (showingPage.workitemFromQuery) {
                    const workitemId = window.location.pathname.replace(/.*_queries\/edit\//, '').replace('/', '');
                    href = conf.workItemUrl.replace('${workitemId}', workitemId);
                    text = (true === conf.includesItemId ? `#${workitemId} ` : '') + document.querySelector('[aria-label="Title Field"]').value
                } else if (showingPage.sprintBoard) {
                    text = getBreadcrumb() + ' > ' + document.querySelector('.internal-breadcrumb--item').innerText + ' > ' + document.querySelector('.sprint-picker-view-action').querySelector('span').innerText
                } else if (showingPage.wiki) {
                    text = getBreadcrumb();
                } else if (showingPage.queries) {
                    text = getBreadcrumb() + ' > ' + getBreadcrumb(1, '.internal-breadcrumb--item');
                } else if (showingPage.release) {
                    text = getBreadcrumb() + ' > ' + document.querySelector('.bolt-header-title').innerText
                } else if (showingPage.releaseProgress) {
                    text = getBreadcrumb();
                } else {
                    text = getBreadcrumb(2);
                }


                conf.debug && console.log(`Devops extentions : [bootstrapCopy] : Copy [${text}](${href})`)

                const range = document.createRange()
                const copyLinkNode = document.getElementById('copyLink')

                copyLinkNode.href = href
                copyLinkNode.text = text
                range.selectNodeContents(copyLinkNode)
                selection.removeAllRanges()
                selection.addRange(range)
                copyMessageNode.children[0].innerText = `[${text}](${href})`
            copyMessageNode.hidden = false;
                setTimeout(() => {
                    copyMessageNode.hidden = true;
                    selection.removeAllRanges()
                }, conf.messageDisplayTime)
            }
        });
        console.log('Devops extentions : [bootstrapCopy] : activated');
    }

    /** end bootstrapCopy */

    function fixQuerryBugInWiki() {
        if (window.location.pathname.includes('/_wiki')) {
            let collapse = document.getElementsByClassName('wiki-collapse-icon-effect')[0];
            collapse.click();
            setTimeout(() => collapse.click(), 1000);
        }
    }

    function loadFixQuerryBugInWiki() {
        addEventListener("load", (event) => {
            fixQuerryBugInWiki()
            window.addEventListener('hashchange', () => {
                fixQuerryBugInWiki()
            })
        });
        console.log('Devops extentions : [fixQuerryBugInWiki] : activated');
    }

    /** showMore */

    function showMore() {
        let node = document.evaluate("//*[text()='" + conf.showMore.label + "']", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
        if (null !== node) {
            console.log("expand", node);
            node.click();
            showMore();
        } else {
            setTimeout(showMore, 1000);
        }
    }

    function loadShowMore() {
        addEventListener("load", (event) => {
            showMore();
        });
        console.log('Devops extentions : [showMore          ] : activated');
    }

    /** end showMore */

    /** releaseNote **/
    function generateReleaseNote() {
        let i = 0;
        let p = '';
        for (let e of document.querySelectorAll('a.bolt-link.no-underline-link[href]')) {
            if (e.href.includes('/_workitems/edit/')) {
                i++; if (0 == i % 2) {
                    p += `#${e.href.substring(e.href.lastIndexOf("/") + 1)} - ${e.text}\n`;
                }
            }
        }
        console.log(p);
    }

    function loadReleaseNote() {
        window.rn = generateReleaseNote;
        console.log('Devops extentions : [releaseNote       ] : activated');
    }

    loadHoursToDays()
    bootstrapCopy();
//    loadFixQuerryBugInWiki();
    loadShowMore();
    loadReleaseNote();
    console.log('Devops extentions : loaded');

})();
