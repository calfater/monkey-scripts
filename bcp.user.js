// ==UserScript==
// @name        BCP - Keep alive
// @namespace   Violentmonkey Scripts
// @match       https://www.banquebcp.fr/espace-client/*
// @grant       none
// @version     1.0
// @author      -
// @description 12/1/2020, 2:10:13 PM
// ==/UserScript==

setInterval(() => {
//  var oReq = new XMLHttpRequest();
//  oReq.withCredentials = true;
//  oReq.addEventListener("load", (t) => {/* console.log(t.target.response) */});
//  oReq.open("GET","https://www.banquebcp.fr/espace-client/compte");
//  oReq.send();
  if (window.location.pathname === "/espace-client/session-handler")  {
    console.log("Session again alive");
    document.getElementsByTagName('button')[0].click();
  } else {
    console.log("Session still alive");
  }
}, 60000);
