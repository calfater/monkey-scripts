// ==UserScript==
// @name        Quick folder move
// @namespace   Violentmonkey Scripts
// @match       https://office.mailbox.org/appsuite/
// @require     https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js
// @grant       none
// @version     1.0
// @author      -
// @description 9/17/2021, 1:30:56 PM
// ==/UserScript==

jQuery.noConflict();

(function($) {

  function createStyle(css) {
    var style = document.createElement('style');
    if (style.styleSheet) {
      style.styleSheet.cssText = css;
    } else {
      style.appendChild(document.createTextNode(css));
    }
    document.getElementsByTagName('head')[0].appendChild(style);
  }

  function wait(duration) {
    return new Promise(resolve => setTimeout(resolve, duration));
  }

  function getFolderList(node, parentPathText) {
    let folders = {};
    if (!node) {
      node = $('div.modal-dialog').find('[data-id="virtual/myfolders"]'); // li
      parentPathText = "";
    }
    $(node).find(' > .subfolders > li').each((_, n) => {
      let libelle = parentPathText + (parentPathText ? '/' : '') + $(n).find('> div > .folder-label > div').text()
      folders[libelle] = n;
      Object.assign(folders, getFolderList($(n), libelle));
    });
    return folders;
  }

  function expandFolders() {
    console.log("Expanding folders")
    return new Promise((resolve, reject) => {
      _expandFolders(resolve, 0)
    });
  }

  function _expandFolders(_resolve, count) {
    setTimeout(() => {
      let expandedCount = $('div.modal-dialog').find('[data-id="virtual/myfolders"]').find('.fa-caret-right').click().length;
      if (0 < expandedCount) {
        _expandFolders(_resolve, count + expandedCount);
      } else {
        console.log(`Expanding folders : ${count} folders exanded`);;
        _resolve();
      }
    }, 4000);
  }


  function cacheFolderList() {
    return new Promise((resolve) => {
      (undefined == window.qfm) && (window.qfm = {});
      window.qfm.folderList = getFolderList();
      console.log(`${Object.keys(window.qfm.folderList).length} folders found`)
      resolve();
    });
  }

  function populateForm() {
    $('div.modal-dialog').find('.modal-header').append($('<div id="qfm"><input id="qfm-input" type="text" placeholder="Dossier" /><ul id="qfm-list"></ul></div>'));
    wait(100).then(() => $('#qfm-input').focus()).then(() => wait(1000)).then(() => $('#qfm-input').focus())

    $('#qfm-input').keyup((e) => {
      if (e.which === 13) {
        $('#qfm-list-selected').click();
      } else if (e.which === 38) {
        $('#qfm-list-selected').attr('id', null).prev().attr('id', 'qfm-list-selected');
      } else if (e.which === 40) {
        $('#qfm-list-selected').attr('id', null).next().attr('id', 'qfm-list-selected');
        return;
      } else {
        $('#qfm-list').empty();
        let search = e.target.value
        let index = 0;
        $(Object.keys(window.qfm.folderList)).each((_, libelle) => {
          if (0 < search.length && libelle.toLowerCase().includes(search.toLowerCase())) {
            index++;
            $('#qfm-list').append($(`<li ${1 == index ? 'id="qfm-list-selected"' : ''}>${libelle}</li>`).click((e) => {
              window.qfm.folderList[e.target.innerText].click();
              wait(500).then(() => $('[data-action="ok"]').click())
            }));
          }
        })
      }
    });
  }

  // function collapseFolderList() {
  //   console.log('Collapsing folders');
  //   $('div.modal-dialog').find('.folder-label').parent().parent().find('.fa-caret-down').click();
  //   console.log('Collapsing folders : done');
  // }

  createStyle(`
      #qfm #qfm-list {
        padding-left: 0;
      }

      #qfm #qfm-list-selected {
        background-color: #1e395421;
      }

      #qfm li {
        list-style-type: none;
        border-radius: 5px;
        padding: 2px 2px 0 5px;
      }
    `);


  $(document).ready(function() {
    $(document).keydown(function(e) {
      if (String.fromCharCode(e.which) === 'M' && e.ctrlKey /* && e.shiftKey*/ ) { // m
        e.stopPropagation();
        $('.classic-toolbar-container').find('li.dropdown.more-dropdown[role="presentation"]').find('a').get(0).click();
        let moves = $('a[data-action="io.ox/mail/actions/move"]')
        moves.get(moves.length - 1).click();

        if (undefined == window.qfm) {
          expandFolders()
            .then(cacheFolderList)
            .then(populateForm);
        } else {
          wait(500).then(cacheFolderList).then(populateForm);
        }
      }

      if (String.fromCharCode(e.which) === 'U' && e.ctrlKey /* && e.shiftKey*/ ) { //u
        e.preventDefault();
        e.stopPropagation();
        $('.classic-toolbar-container').find('li.dropdown.more-dropdown[role="presentation"]').find('a').get(0).click();
        $('a[data-action="io.ox/mail/actions/mark-unread"]').get(0).click()
      }
    })
  });
  console.log("Quick folder move loaded");
})(jQuery);
